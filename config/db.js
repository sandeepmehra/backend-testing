const mongoose = require("mongoose");
require('dotenv').config();
module.exports = connect = async ()=>{
    try{
        await mongoose.connect(process.env.MONGO_DB_URL,
            { 
                useNewUrlParser: true, 
                useUnifiedTopology: true,   
            }
        );
        console.log("Sucessfully DB Connected");
    }  catch (error){
        console.log("DB not Connected :", error);
    }
}
