require('dotenv').config();
const Patient = require("../models/Patient");

module.exports.createPost = async(req, res)=>{
    try {
        const response = await Patient.create(req.body);
        return res.status(201).json({
            msg: 'Your Form have been submitted',
            response
        });
    } catch (error) {
        return res.status(500).json({errors: error, msg: error.message})
    }
};

module.exports.fetchPosts = async (req, res) => {
    try {
        const response = await Patient.find({});
        return res.status(200).json({response});
    } catch (error) {
        return res.status(500).json({errors: error, msg: error.message});
    }
};
