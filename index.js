const express = require("express");
const path = require("path");
const bodyParser = require('body-parser');
const connect = require("./config/db");
const cors = require('cors');
const { createPost, fetchPosts } = require("./controllers/postController");
const app = express();

require('dotenv').config();
//calling database to connect
connect();
 
// Body-parser middleware
app.use(bodyParser.urlencoded({extended:false}))
app.use(bodyParser.json());
  
app.use(
    cors({
        credentials: true,
        allowedHeaders: ['Authorization','Origin','X-Requested-With','Content-Type','Accept','X-Access-Token', 'access-control-allow-credentials'],
        exposedHeaders: ['Authorization','Origin','X-Requested-With','Content-Type','Accept','X-Access-Token','access-control-allow-credentials'],
        methods: 'GET,PUT,POST,DELETE,OPTIONS,HEAD,PATCH',
        origin: "*",
        preflightContinue: false,
        optionsSuccessStatus: 200,
    })
);
  
app.get("/", fetchPosts);
app.post("/create", createPost);

const PORT = process.env.PORT || '8000';

app.listen(PORT,()=>{
    console.log('The Port is running : ', PORT);
});