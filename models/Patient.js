const {Schema, model} = require("mongoose");

const patientSchema = new Schema({
    name: {
        type: String,
        required: true,
    },
    dobOrAge: {
        type: String, 
        required: true,
    },
    sex: {
        type: String, 
        required: true,
    },
    mobile: {
        type: String,
        required: false,
    },
    address: {
        type: String,
        required: false,
    },
    state: {
        type: String,
        required: false,
    },
    city: {
        type: String,
        required: false,
    },
    country: {
        type: String,
        required: false,
    },
    pincode: {
        type: String,
        required: false,
    },
    id_type: {
        type: String,
        required: false,
    },
    guardian_label: {
        type: String,
        required: false,
    },
    govt_id: {
        type: String,
        required: false,
    },
    guardian_name: {
        type: String,
        required: false,
    },
    occupation: {
        type: String,
        required: false,
    },  
    email: {
        type: String,
        required: false,
    },
    emergency_contact_number: {
        type: String,
        required: false,
    },
    nationality: {
        type: String,
        required: false,
    },
}, 
{timestamps: true}
);
module.exports = model("patient", patientSchema);